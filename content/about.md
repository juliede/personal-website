+++
title = "About"
+++

## About Me

{{< figure class="avatar" src="/avatar.jpg" >}}

Hello and welcome! I'm a cybersecurity engineer, with a background in 
networking and Linux system administration. Over the past 5 years, I've worked
in the OT cyber security department at Capgemini. I enjoy doing CTF challenges 
and attending cyber-security events. Contact me to learn more!


## Work experience and training

*_Security testing in aeronautics (air and ground systems):_*
* Audit of industrial OS and network applications : development of attack tools 
(python, bash), recommendations on system hardening (Linux / Windows / various 
applications).
* Configuration audits : standard system and network applications (remote 
management, databases…), kernel and OS configuration, based on the NIST, CIS and 
ANSSI (French national cybersecurity agency) standards.
* Vulnerability analysis.
* Definition of testing objectives, preparation of test reports.

*_Industrial systems audit (SCADA):_*
* Pentest (energy sector, robotics, ...)
* Configuration audit (Linux, Windows, Cisco, Fortinet, pfSense...)
* Network architecture audit (ANSSI, NIST, IEC-62351,...).

*_Vulnerability watch and analysis on hardened industrial OS:_*
* Development of automation tools
* Vulnerability analysis

*_PoC - Industrial network monitoring:_*
* Set up a SCADA network architecture (including various PLCs and supervision software)
* Development of attack scenarios and tools (Modbus, S7, IEC-104, OPC DA...)
* Integration of a popular SIEM, analysis of its efficiency

*_Other assignments (lab support team):_*
- System administration Linux / Windows.
- Deployment of an open source collaborative tool to optimize technology and 
vulnerability watch.
- Install and maintain a source forge and wiki (Gitlab) 

*_Training_*
- SANS Institute - SEC506: Securing Linux/Unix (2018)


## Capture The Flag

You can find me on popular hacking platforms (codename "mireille"): 
- [Root-Me](https://www.root-me.org/): 4360 pts, ranked 838th/206102.
- [Hack the box](https://www.hackthebox.eu/badge/image/11529)

I've also participated in CTF events:
- SSTIC 2016
- [THCON 2017](https://ctftime.org/event/415): Pony_n_Clyde team
- [SSTIC 2020](https://www.sstic.org/2020/challenge/)

## Cybersecurity conferences

I usually try to attend (in person or online) the following events:
- Symposium sur la sécurité des technologies de l'information et des communications (SSTIC), Rennes, France
- Toulouse Hacking Convention (THCON), Toulouse, France
- INSA Toulouse annual event "Journée sur la sécurité informatique", Toulouse, France
- Defcon, Black Hat,... depending on the topic


## Other IT / non IT related activities

Up until the pandemic, I was a member of "La Chouette Coop", a food coop based 
in Toulouse. With friends, we set up the internal IT infrastructure (hardware 
and network). We also set up a documentation server.

I've been playing basketball for years. I also enjoy hiking and sewing.


## Credits

This is a Hugo based resume template. You can find the full source code on
[GitHub](https://github.com/ojroques/hugo-researcher).

Thanks to denikasapovic for the avatar.
